//jquery
import * as $ from "jquery";
import "jquery-ui-bundle/jquery-ui";
import "jquery-ui-bundle/jquery-ui.css";
// basthon
import { GUIBase, GUIOptions } from "@basthon/gui-base";
const dialog = require("../js/base/js/dialog");

// Blockly
import BlockEditor from "./blocks";

// fontawesome
import "@fortawesome/fontawesome-free/css/all.css";

/**
 * Basthon part of the notebook GUI.
 */
export class GUI extends GUIBase {
  private _notebook?: any;
  private _events?: any;
  private _blockEditor: BlockEditor;

  public constructor(options: GUIOptions) {
    super(
      (() => {
        options.uiName = "notebook";
        return options;
      })()
    );
    this._contentFilename = "Untitled.ipynb";
    this._urlKey = "ipynb";

    /* register extensions */
    const admonitionLoader = async () => {
      const marked = require("marked");
      const admonition = (await import("marked-admonition-extension")).default;
      //@ts-ignore
      import("../css/admonition.css");
      marked.use(admonition);
    };
    this.registerExtension("admonition", admonitionLoader);
    this.registerExtension("admonitions", admonitionLoader);

    this._blockEditor = new BlockEditor();
  }

  /**
   * Get notebook's content.
   */
  public content(): string {
    return JSON.stringify(this._notebook.toJSON());
  }

  /**
   * Set notebook's content.
   */
  public setContent(content: string): void {
    if (!content) return;
    let ipynb: string;
    try {
      ipynb = JSON.parse(content);
    } catch (e: any) {
      throw new Error(
        `Impossible d'ouvrir le notebook : l'ipynb est corrompu.\n${e.toString()}`
      );
    }
    try {
      this._notebook.fromJSON(ipynb);
    } catch (e: any) {
      throw new Error(
        `Impossible d'ouvrir le notebook : l'ipynb n'est pas conforme.\n${e.toString()}`
      );
    }
  }

  /**
   * Notify the user with an error.
   */
  public error(title: string, message: string) {
    dialog.modal({
      notebook: this._notebook,
      keyboard_manager: this._notebook?.keyboard_manager,
      title: title,
      body: $("<div>").html(message),
      buttons: {
        OK: {
          class: "btn-danger",
        },
      },
    });
  }

  /**
   * Notify the user.
   */
  public info(title: string, message: string) {
    dialog.modal({
      notebook: this._notebook,
      keyboard_manager: this._notebook?.keyboard_manager,
      title: title,
      body: $("<div>").html(message),
      buttons: {
        OK: {
          class: "btn-primary",
        },
      },
    });
  }

  /**
   * Ask the user to confirm or cancel.
   */
  public confirm(
    title: string,
    message: string,
    text: string,
    callback: () => void,
    textCancel: string,
    callbackCancel: () => void
  ): void {
    dialog.modal({
      notebook: this._notebook,
      keyboard_manager: this._notebook.keyboard_manager,
      title: title,
      body: $("<div>").html(message),
      buttons: {
        [text]: {
          class: "btn-primary",
          click: callback,
        },
        [textCancel]: {
          click: callbackCancel,
        },
      },
    });
  }

  /**
   * Ask the user to select a choice.
   */
  public select(
    title: string,
    message: string,
    choices: {
      text: string;
      handler: () => void;
    }[],
    textCancel: string,
    callbackCancel: () => void
  ): void {
    // build select menu
    let selected = 0;
    const selectMenu = $(
      '<div class="list-group" style="max-width: 200px; margin: auto; margin-top: 10px;">'
    );
    choices.forEach((c, i) => {
      const item = $('<a href="#">').html(c.text);
      item.addClass("list-group-item");
      item.addClass("list-group-item-action");
      if (i == 0) item.addClass("active");
      item.click(() => {
        item.parent().find("a").removeClass("active");
        item.addClass("active");
        selected = i;
      });
      selectMenu.append(item);
    });
    const body = $("<div>").append($("<p>").html(message)).append(selectMenu);
    dialog.modal({
      notebook: this._notebook,
      keyboard_manager: this._notebook.keyboard_manager,
      title: title,
      body: body,
      buttons: {
        OK: {
          class: "btn-primary",
          click: () => {
            const handler = choices[selected].handler;
            if (handler != null) handler();
          },
        },
        [textCancel]: {
          click: callbackCancel || (() => { }),
        },
      },
    });
  }

  /**
   * Get current darkmode.
   */
  private async _getDarkmode(): Promise<boolean> {
    return await this.getState("darkmode", false);
  }

  /**
   * Get mode as a string (dark/light).
   */
  public async theme() {
    const darkmode = await this._getDarkmode();
    return darkmode ? "dark" : "light";
  }

  /**
   * Switch dark/light mode.
   */
  public async switchDarkLight() {
    const darkmode = await this._getDarkmode();
    await this.setState("darkmode", !darkmode);
    await this.updateDarkLight();
  }

  /**
   * Update dark/light appearence.
   */
  public async updateDarkLight() {
    const darkmode = await this._getDarkmode();
    const mode = darkmode ? "dark" : "light";
    this._notebook?.set_theme(mode);
    this._blockEditor?.changeTheme(mode);
  }

  protected async setupUI(options: any) {
    this._notebook = options?.notebook;
    this._contentFilename =
      this._notebook.notebook_name ?? this._contentFilename;
    await super.setupUI(options);

    await Promise.all([this._blockEditor.init()]);

    await this.updateDarkLight();

    // avoiding notebook loading failure.
    if (!this._notebook) location.reload();

    // keeping back events from notebook.
    this._events = this._notebook.events;

    if (!this._notebook._fully_loaded) {
      await new Promise((resolve, reject) =>
        this._events.on("notebook_loaded.Notebook", resolve)
      );
    }

    // alert when opening old Python 3.8 notebook
    if (this.language === "python3-old") {
      const callback = () => {
        const url = new URL(window.location.href);
        url.searchParams.set("kernel", "python3");
        window.location.href = url.toString();
      };
      this.confirm(
        "Ouvrir avec la dernière version de Basthon ?",
        "Ce notebook utilise une version ancienne du" +
        " noyau de Basthon (Python 3.8) qui ne sera " +
        "bientôt plus maintenue. Voulez-vous utiliser " +
        "la dernière version du noyau (Python 3.10) ?",
        "Utiliser Python 3.10",
        callback,
        "Rester avec Python 3.8",
        () => { }
      );
    }



    // load the first cell
    // this.getCellAndUpdateWorkspace();

    // add buttons
    this.addButtons();

    this._blockEditor._workspace.addChangeListener(() => {
      // trigger only on mouse up
      if (this._blockEditor._workspace.isDragging()) return;
      this.updateCode();
    });

    // listen to button click
    let button = document.getElementById("get-current-cell") as HTMLElement;
    button?.addEventListener("click", () => this.getCell());

    button = document.getElementById("delete_cell") as HTMLElement;
    button?.addEventListener("click", () => this.deleteCell());

    button = document.getElementById("btn_langage") as HTMLElement;
    button?.addEventListener("click", () => this.switchLanguage());

    // detect cell change
    this._notebook.events.on("selected_cell_type_changed.Notebook", () => {
      this.getCellAndUpdateWorkspace();
    });

    // detect cell selection
    this._notebook.events.on("select.Cell", () => {
      this._notebook.keyboard_manager.enable();
    });
    // disable notebook shortcuts 
    this._blockEditor._workspace.addChangeListener(this.disableShortcuts.bind(this));
  }

  public async disableShortcuts(event : any) {
    if (event.type == "click" || event.type == "drag" || event.type == "selected") {
      this._notebook.keyboard_manager.disable();
    }
  }

  public async getCell() {
    await this.loaded();
    console.log(this._notebook.get_selected_cell());
  }

  public async deleteCell() {
    await this.loaded();
    const cell_number = this._notebook.get_selected_index();
    this._notebook.delete_cell(cell_number);
    this._blockEditor.removeWorkspace(cell_number);
  }

  public async getCellAndUpdateWorkspace() {
    await this.loaded();

    //  Clear the workspace
    this._blockEditor.clearWorkspace();

    // get the cell workspace metadata
    const cell = this._notebook.get_selected_cell();
    const metadata = cell.metadata;
    const workspace = metadata["workspace"];

    this._blockEditor.loadWorkspace(workspace);
  }

  public async updateCode() {
    await this.loaded();

    // test if the selected cell is a code cell
    const cell = this._notebook.get_selected_cell();
    let id = this._notebook.get_selected_index();
    // console.log(
    //   "cell id is " + id + " and cell type is " + cell.cell_type + ""
    // );

    if (cell.cell_type == "code" && id != null) {
      const code = this._blockEditor.getCode();
      cell.set_text(code);

      // save the workspace to cell metadata
      const workspace = await this._blockEditor.download();
      const metadata = cell.metadata;
      metadata["workspace"] = workspace;
      cell.metadata = metadata;
    }
  }

  public async saveContent(content: any) {
    // save the content to the local storage
    this.setState("notebook_content", content);
  }

  /**
   * Sharing notebook via URL.
   */
  public async share() {
    this._events.trigger("before_share.Notebook");
    super.share();
    this._events.trigger("notebook_shared.Notebook");
  }

  /**
   * Load the content of a Python script in first cell.
   */
  public async loadPythonInCell(file: File): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsText(file);
      reader.onload = async (event) => {
        const new_cell = this._notebook.insert_cell_above("code", 0);
        new_cell.set_text(event?.target?.result);
        // notification seems useless here.
        resolve();
      };
      reader.onerror = reject;
    });
  }

  /**
   * Open *.py file by asking user what to do:
   * load in notebook cell or put on (emulated) local filesystem.
   */
  public async openPythonFile(file: File) {
    const msg = $("<div>").html("Que faire de " + file.name + " ?");
    this.confirm(
      "Que faire du fichier ?",
      msg,
      "Charger dans le notebook",
      () => {
        this.loadPythonInCell(file);
      },
      "Installer le module",
      () => {
        this.putFSRessource(file);
      }
    );
  }

  /**
   * Opening file: If it has .ipynb extension, load the notebook,
   * if it has .py extension, loading it in the first cell
   * or put on (emulated) local filesystem (user is asked to),
   * otherwise, loading it in the local filesystem.
   */
  public async openFile() {
    return await this._openFile({
      py: this.openPythonFile.bind(this),
      ipynb: this.open.bind(this),
    });
  }



  public async switchLanguage() {
    console.log("switching language");
    this.updateDarkLight();
    // Remove the existing event listener



    var language = this._blockEditor.changeLanguage();
    if (language == "python") {
      document.getElementById("btn_langage")!.innerHTML = "<i class=\"fas fa-language\"></i>";
      document.getElementById("btn_langage")!.title = "Traduire les blocs en langage naturel";
    } else {
      document.getElementById("btn_langage")!.innerHTML = "<i class=\"fab fa-python\"></i>";
      document.getElementById("btn_langage")!.title = "Traduire les blocs en Python";
    }
    this.refreshAddChangeListener();
  }


  /** 
  * Add additionnal buttons
  */

  public async addButtons() {

    var toolbar = document.getElementById("maintoolbar-container") as HTMLElement;
    var blocklyDiv = document.getElementById("blocklyDiv") as HTMLElement;
    var notebook_container = document.getElementById("notebook_container") as HTMLElement;

    //  Create a button group
    var button_group = document.createElement("div");
    button_group.className = "btn-group";
    button_group.setAttribute('id', "blockly-buttons");
    button_group.setAttribute('style', "border-radius: 5px; margin-left: 5px; margin-right: 5px;");
    button_group.setAttribute('role', "group");

    // Add translate button to main toolbar
    var button_lang = document.createElement("button");
    button_lang.innerHTML = "<i class='fas fa-language fa-fw'></i>";
    button_lang.title = "Traduire les blocs en langage naturel";
    button_lang.id = "btn_langage";
    button_lang.className = "btn btn-default";
    // add to button group
    button_group.appendChild(button_lang);

    // Add button to hide the blocks
    var button_hide = document.createElement("button");
    button_hide.innerHTML = "<i class='fa fa-eye-slash fa-fw'></i>";
    button_hide.title = "Masquer les blocs";
    button_hide.id = "hide-blocks";
    button_hide.className = "btn btn-default";
    button_hide.onclick = function () {
      blocklyDiv.style.display = "none";
      button_hide.style.display = "none";
      button_show.style.display = "inline";

      // Make div full width
      notebook_container.className =
        "col-12 col-sm-12 col-md-12 scrollit";

      // Resize notebook
      var event = new Event("resize");
      window.dispatchEvent(event);

    };
    // add to button group
    button_group.appendChild(button_hide);

    // Add button to show the blocks
    var button_show = document.createElement("button");
    button_show.innerHTML = "<i class='fa fa-eye'></i>";
    button_show.title = "Afficher les blocs";
    button_show.id = "show-blocks";
    button_show.className = "btn btn-default";
    button_show.setAttribute('style', "display: none;");
    button_show.onclick = function () {
      blocklyDiv.style.display = "block";
      button_hide.style.display = "inline";
      button_show.style.display = "none";

      // Make div half width
      notebook_container.className =
        "col-12 col-sm-12 col-md-6 scrollit";

      // Resize notebook
      var event = new Event("resize");
      window.dispatchEvent(event);
    };
    // add to button group  
    button_group.appendChild(button_show);

    toolbar.appendChild(button_group);
  }

  public async refreshAddChangeListener() {
    this._blockEditor._workspace.addChangeListener(() => {
      // trigger only on mouse up
      if (this._blockEditor._workspace.isDragging()) return;
      this.updateCode();
    });

    // detect cell change
    this._notebook.events.on("selected_cell_type_changed.Notebook", () => {
      this.getCellAndUpdateWorkspace();
    });

    // detect cell selection
    this._notebook.events.on("select.Cell", () => {
      this._notebook.keyboard_manager.enable();
    });

    // disable notebook shortcuts 
    this._blockEditor._workspace.addChangeListener(this.disableShortcuts.bind(this));
  }
}
