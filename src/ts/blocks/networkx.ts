import * as Blockly from "blockly";

export function setNetworkx() {
    var networkx_graph = {
        "message0": Blockly.Msg["networkx_graph"],
        "args0": [
          { "type": "field_variable", "name": "variable", "variable": "reseau_social" },
        ],
      }
    Blockly.Blocks["networkx_graph"] = {
        init: function () {
            this.jsonInit(networkx_graph);
            this.setInputsInline(true);
            this.setPreviousStatement(true, null);
            this.setNextStatement(true, null);
            this.setColour("%{BKY_NETWORKX_COLOR}");
            this.setTooltip("");
            this.setHelpUrl("");
        },
    };

    // Draw
    var networkx_draw = {
        "message0": Blockly.Msg["networkx_draw"],
        "args0": [
            { "type": "field_variable", "name": "variable", "variable": "reseau_social" },
            { "type": "input_value", "name": "with_label", "check": null },
        ],
    };
    Blockly.Blocks["networkx_draw"] = {
        init: function () {
            this.jsonInit(networkx_draw);
            this.setInputsInline(true);
            this.setPreviousStatement(true, null);
            this.setNextStatement(true, null);
            this.setColour("%{BKY_NETWORKX_COLOR}");
            this.setTooltip("");
            this.setHelpUrl("");
        },
    };

    // Add edge
    var networkx_add_edge = {
        "message0": Blockly.Msg["networkx_add_edge"],
        "args0": [
            { "type": "field_variable", "name": "variable", "variable": "reseau_social" },
            { "type": "field_input", "name": "add_edge_value", "text": "A" },
            { "type": "field_input", "name": "add_edge_value2", "text": "B" },
        ],
    };

    Blockly.Blocks["networkx_add_edge"] = {
        init: function () {
            this.jsonInit(networkx_add_edge);
            this.setInputsInline(true);
            this.setPreviousStatement(true, null);
            this.setNextStatement(true, null);
            this.setColour("%{BKY_NETWORKX_COLOR}");
            this.setTooltip("");
            this.setHelpUrl("");
        },
    };

    // Add node
    var networkx_add_node = {
        "message0": Blockly.Msg["networkx_add_node"],
        "args0": [
            { "type": "field_variable", "name": "variable", "variable": "reseau_social" },
            { "type": "field_input", "name": "add_node_value", "text": "A" },
        ],
    };

    Blockly.Blocks["networkx_add_node"] = {
        init: function () {
            this.jsonInit(networkx_add_node);
            this.setInputsInline(true);
            this.setPreviousStatement(true, null);
            this.setNextStatement(true, null);
            this.setColour("%{BKY_NETWORKX_COLOR}");
            this.setTooltip("");
            this.setHelpUrl("");
        },
    };

    // Number of nodes
    var networkx_number_of_nodes = {
        "message0": Blockly.Msg["networkx_number_of_nodes"],
        "args0": [
            { "type": "field_variable", "name": "variable", "variable": "reseau_social" }
        ],
    };

    Blockly.Blocks["networkx_number_of_nodes"] = {
        init: function () {
            this.jsonInit(networkx_number_of_nodes);
            this.setInputsInline(true);
            this.setOutput(true, null);
            this.setColour("%{BKY_NETWORKX_COLOR}");
            this.setTooltip("");
            this.setHelpUrl("");
        },
    };

    // Number of edges
    var networkx_number_of_edges = {
        "message0": Blockly.Msg["networkx_number_of_edges"],
        "args0": [
            { "type": "field_variable", "name": "variable", "variable": "reseau_social" }
        ],
    };

    Blockly.Blocks["networkx_number_of_edges"] = {
        init: function () {
            this.jsonInit(networkx_number_of_edges);
            this.setInputsInline(true);
            this.setOutput(true, null);
            this.setColour("%{BKY_NETWORKX_COLOR}");
            this.setTooltip("");
            this.setHelpUrl("");
        },
    };

    // Diameter
    var networkx_diameter = {
        "message0": Blockly.Msg["networkx_diameter"],
        "args0": [
            { "type": "field_variable", "name": "variable", "variable": "reseau_social" }
        ],
    };

    Blockly.Blocks["networkx_diameter"] = {
        init: function () {
            this.jsonInit(networkx_diameter);
            this.setInputsInline(true);
            this.setOutput(true, null);
            this.setColour("%{BKY_NETWORKX_COLOR}");
            this.setTooltip("");
            this.setHelpUrl("");
        },
    };

    // Radius
    var networkx_radius = {
        "message0": Blockly.Msg["networkx_radius"],
        "args0": [
            { "type": "field_variable", "name": "variable", "variable": "reseau_social" }
        ],
    };

    Blockly.Blocks["networkx_radius"] = {
        init: function () {
            this.jsonInit(networkx_radius);
            this.setInputsInline(true);
            this.setOutput(true, null);
            this.setColour("%{BKY_NETWORKX_COLOR}");
            this.setTooltip("");
            this.setHelpUrl("");
        },
    };

    // Center
    var networkx_center = {
        "message0": Blockly.Msg["networkx_center"],
        "args0": [
            { "type": "field_variable", "name": "variable", "variable": "reseau_social" }
        ],
    };

    Blockly.Blocks["networkx_center"] = {
        init: function () {
            this.jsonInit(networkx_center);
            this.setInputsInline(true);
            this.setOutput(true, null);
            this.setColour("%{BKY_NETWORKX_COLOR}");
            this.setTooltip("");
            this.setHelpUrl("");
        },
    };
}